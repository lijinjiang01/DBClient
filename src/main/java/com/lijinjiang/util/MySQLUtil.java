package com.lijinjiang.util;

/**
 * @ClassName MySQLUtil
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:17
 * @ModifyDate 2022/8/15 16:17
 * @Version 1.0
 */
public class MySQLUtil {
    // 表类型
    public static final String TABLE_TYPE = "BASE TABLE";

    // 系统视图
    public static final String SYSTEM_VIEW_TYPE = "SYSTEM VIEW";

    // 存储过程
    public static final String PROCEDURE_TYPE = "PROCEDURE";
}
