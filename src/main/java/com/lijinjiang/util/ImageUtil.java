package com.lijinjiang.util;

import javax.swing.*;
import java.awt.*;

/**
 * @ClassName ImageUtil
 * @Description 图标工具类
 * @Author Li
 * @Date 2022/8/11 17:23
 * @ModifyDate 2022/8/11 17:23
 * @Version 1.0
 */
public class ImageUtil {
    public static String PATH = ImageUtil.class.getResource("/").getPath();

    // 项目 logo
    public final static ImageIcon LOGO_ICON = new ImageIcon(PATH + "images/frame/logo_icon.png");
    public final static Image LOGO_IMAGE = new ImageIcon(PATH + "images/frame/logo.png").getImage();

    // 关闭图片
    public final static Image CLOSE_IMAGE = new ImageIcon(PATH + "images/frame/close.png").getImage();
    public final static ImageIcon CLOSE_ICON = new ImageIcon(ImageUtil.CLOSE_IMAGE.getScaledInstance(22, 22, Image.SCALE_SMOOTH));
    public final static Image CLOSE_ENTERED_IMAGE = new ImageIcon(PATH + "images/frame/close_entered.png").getImage();
    public final static ImageIcon CLOSE_ENTERED_ICON = new ImageIcon(ImageUtil.CLOSE_ENTERED_IMAGE.getScaledInstance(22, 22, Image.SCALE_SMOOTH));

    /**
     * 树分类
     */
    // 创建连接的Image
    public final static Icon CONNECTION_ICON = new ImageIcon(PATH + "images/frame/connection.png");

    // 树中服务器已连接图片
    public static final Icon CONNECTION_OPEN_ICON = new ImageIcon(PATH + "images/tree/connection_open.png");
    public static final Image CONNECTION_OPEN_IMAGE = new ImageIcon(PATH + "images/tree/connection_open.png").getImage();

    // 树中服务器未连接图片
    public static final Icon CONNECTION_CLOSE_ICON = new ImageIcon(PATH + "images/tree/connection_close.png");

    // 树中数据库已连接图片
    public static final Icon DATABASE_OPEN_ICON = new ImageIcon(PATH + "images/tree/database_open.png");
    public static final Image DATABASE_IMAGE = new ImageIcon(PATH + "images/tree/database_open.png").getImage();

    // 树中数据库未连接图片
    public static final Icon DATABASE_CLOSE_ICON = new ImageIcon(PATH + "images/tree/database_close.png");

    // 树中表节点的图标
    public static final Icon TABLE_ICON = new ImageIcon(PATH + "images/tree/table.png");

    // 树中视图节点的图标
    public static final Icon VIEW_ICON = new ImageIcon(PATH + "images/tree/view.png");

    // 树存储过程的图标
    public static final Icon PROCEDURE_ICON = new ImageIcon(PATH + "images/tree/procedure.png");

    // 查询图标
    public static final Icon QUERY_ICON = new ImageIcon(PATH + "images/tree/query.png");

    /**
     * 其他分类
     */
    // 保存图标
    public static final Icon RUN_ICON = new ImageIcon(PATH + "images/other/run.png");
    // 保存图标
    public static final Icon SAVE_ICON = new ImageIcon(PATH + "images/other/save.png");
}
