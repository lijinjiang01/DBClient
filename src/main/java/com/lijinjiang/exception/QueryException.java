package com.lijinjiang.exception;

/**
 * @ClassName QueryException
 * @Description TODO
 * @Author Li
 * @Date 2022/8/27 15:50
 * @ModifyDate 2022/8/27 15:50
 * @Version 1.0
 */
public class QueryException extends RuntimeException {
    public QueryException(String message) {
        super(message);
    }
}
