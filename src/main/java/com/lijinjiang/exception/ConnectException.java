package com.lijinjiang.exception;

/**
 * @ClassName ConnectException
 * @Description TODO
 * @Author Li
 * @Date 2022/8/30 23:31
 * @ModifyDate 2022/8/30 23:31
 * @Version 1.0
 */
public class ConnectException extends RuntimeException {
    public ConnectException(String message) {
        super(message);
    }
}
