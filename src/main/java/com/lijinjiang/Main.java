package com.lijinjiang;

import com.lijinjiang.beautyeye.BeautyEyeLNFHelper;
import com.lijinjiang.util.ImageUtil;
import com.lijinjiang.util.SystemUtil;
import com.lijinjiang.view.MainFrame;

import javax.swing.*;
import javax.swing.plaf.FontUIResource;
import java.awt.*;

/**
 * @ClassName Main
 * @Description 主方法
 * @Author Li
 * @Date 2022/8/10 10:56
 * @ModifyDate 2022/8/10 10:56
 * @Version 1.0
 */
public class Main {
    public static void main(String[] args) {
        try {
            /**
             * 设置本属性将改变窗口边框样式定义
             * 系统默认样式 : osLookAndFeelDecorated
             * 强立体半透明 : translucencyAppleLike
             * 弱立体半透明 : translucencySmallShadow
             * 普通不透明 : generalNoTranslucencyShadow
             */
            BeautyEyeLNFHelper.frameBorderStyle = BeautyEyeLNFHelper.FrameBorderStyle.generalNoTranslucencyShadow;
            BeautyEyeLNFHelper.launchBeautyEyeLNF();
            // 设置默认 ICON
            UIManager.put("Frame.icon", ImageUtil.LOGO_ICON);
            // 调整默认字体
            String[] font = SystemUtil.DEFAULT_FONT;
            for (int i = 0; i < font.length; i++)
                UIManager.put(font[i], new FontUIResource("微软雅黑", Font.PLAIN, 13));
            // 初始化主窗口
            new MainFrame();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(e.getMessage());
        }
    }
}
