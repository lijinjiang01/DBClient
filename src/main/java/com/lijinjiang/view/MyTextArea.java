package com.lijinjiang.view;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;
import java.awt.*;

/**
 * @ClassName MyTextArea
 * @Description TODO
 * @Author Li
 * @Date 2022/8/28 16:26
 * @ModifyDate 2022/8/28 16:26
 * @Version 1.0
 */
public class MyTextArea extends JPanel {
    private LinePanel linePanel;

    private JTextArea textArea;

    private JScrollPane scrollPane;

    public MyTextArea() {
        this.setLayout(new BorderLayout());
        linePanel = new LinePanel();
        linePanel.setPreferredSize(new Dimension(25, 10));
        this.textArea = new JTextArea() {
            public void paint(Graphics g) {
                super.paint(g);
                linePanel.repaint();
            }
        };
        this.scrollPane = new JScrollPane(this.textArea);
        this.add(linePanel, BorderLayout.WEST);
        this.add(scrollPane, BorderLayout.CENTER);
        this.textArea.setBorder(null);
        this.scrollPane.setBorder(null);
    }

    // 获取文本
    public String getText() {
        return textArea.getText();
    }

    // 内部类：显示行号的面板
    private class LinePanel extends JPanel {
        @Override
        public void paint(Graphics g) {
            super.paint(g);
            // starting pos in document
            int start = textArea.viewToModel(scrollPane.getViewport().getViewPosition());
            // end pos in doc
            int end = textArea.viewToModel(new Point(scrollPane.getViewport().getViewPosition().x + textArea.getWidth()
                    , scrollPane.getViewport().getViewPosition().y + textArea.getHeight()));

            // translate offsets to lines
            Document doc = textArea.getDocument();
            int startLine = doc.getDefaultRootElement().getElementIndex(start) + 1;
            int endLine = doc.getDefaultRootElement().getElementIndex(end) + 1;

            int fontHeight = g.getFontMetrics(textArea.getFont()).getHeight();
            int fontDesc = g.getFontMetrics(textArea.getFont()).getDescent();
            int starting_y = -1;

            try {
                starting_y = textArea.modelToView(start).y -
                        scrollPane.getViewport().getViewPosition().y + fontHeight - fontDesc;
            } catch (BadLocationException ble) {
                ble.printStackTrace();
            }

            for (int line = startLine, y = starting_y; line <= endLine; y += fontHeight, line++) {
                g.drawString(Integer.toString(line), getNumX(line), y);
            }
        }

        // 获取当前行的行号 x 坐标
        private int getNumX(int line) {
            int width = linePanel.getWidth() - 5;
            FontMetrics metrics = linePanel.getFontMetrics(this.getFont());
            int fontHeight = metrics.getHeight();
            int fontWidth = metrics.stringWidth("0");
            int digit = 0;
            while (line > 0) {
                line = line / 10;
                digit++;
            }
            int x = width - digit * fontWidth;
            // 自适应调整宽度
            if (x < 5) {
                linePanel.setPreferredSize(new Dimension(getWidth() + fontWidth, fontHeight));
                linePanel.updateUI();
            }
            return x;
        }
    }
}
