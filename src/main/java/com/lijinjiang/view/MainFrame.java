package com.lijinjiang.view;

import com.lijinjiang.model.GlobalContext;
import com.lijinjiang.model.tree.ServerConnection;
import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.List;

/**
 * @ClassName MainFrame
 * @Description TODO
 * @Author Li
 * @Date 2022/8/10 11:00
 * @ModifyDate 2022/8/10 11:00
 * @Version 1.0
 */
public class MainFrame extends JFrame {
    // 全局上下文环境
    private GlobalContext ctx;

    // 主界面的分隔pane
    private JSplitPane mainPane;

    // 左面板
    private LeftPane leftPane;

    // 右面板
    private RightPane rightPane;

    public LeftPane getLeftPane() {
        return leftPane;
    }

    public void setLeftPane(LeftPane leftPane) {
        this.leftPane = leftPane;
    }

    public RightPane getRightPane() {
        return rightPane;
    }

    public void setRightPane(RightPane rightPane) {
        this.rightPane = rightPane;
    }

    // 选项卡面板，位于mainPane的右侧
    /* private RightPane rightPane;*/

    // 无参构造方法
    public MainFrame() {
        this.setTitle("DBClient");
        this.setIconImage(ImageUtil.LOGO_IMAGE); // 设置 logo
        this.setSize(new Dimension(1200, 900)); // 默认大小
        this.initContext(); // 初始化全局配置
        this.createMenuBar(); // 创建菜单栏
        this.createToolBar(); // 创建工具栏
        this.createMainPane(); // 创建主面板
        this.setLocationRelativeTo(null); // 设置居中
        this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE); // 设置执行关闭时的默认操作
        this.setVisible(true); // 设置可见
    }

    /**
     * 初始化全局配置
     */
    private void initContext() {
        this.ctx = new GlobalContext();
        List<ServerConnection> scs = this.ctx.getPropertiesHandler().getAllSC();
        for (ServerConnection sc : scs) {
            this.ctx.addSC(sc);
        }
    }

    /**
     * 创建菜单栏
     */
    private void createMenuBar() {
        JMenuBar menuBar = new JMenuBar(); // 菜单栏
        JMenu fileMenu = new JMenu("文件"); // 文件栏
        JMenuItem newSCItem = new JMenuItem("新建连接");
        newSCItem.addActionListener(e -> {
            new SCDialog(ctx, this);
        });
        fileMenu.add(newSCItem);
        fileMenu.addSeparator();
        JMenuItem quitItem = new JMenuItem("退出 DBClient");
        quitItem.addActionListener(e -> {
            System.exit(0);
        });
        fileMenu.add(quitItem);
        JMenu helpMenu = new JMenu("帮助"); //帮助栏
        JMenuItem aboutItem = new JMenuItem("关于");
        aboutItem.addActionListener(e -> {
            new AboutDialog(this);
        });
        helpMenu.add(aboutItem);

        menuBar.add(fileMenu);
        menuBar.add(helpMenu);
        this.setJMenuBar(menuBar);
    }

    /**
     * 创建工具栏
     */
    private void createToolBar() {
        JToolBar toolBar = new JToolBar();
        // 新建连接按钮
        Action newSCAction = new AbstractAction("", ImageUtil.CONNECTION_ICON) {
            @Override
            public void actionPerformed(ActionEvent e) {
                new SCDialog(ctx, MainFrame.this);
            }
        };
        toolBar.add(newSCAction);
        toolBar.setFloatable(false); // 设置不可拖动
        this.add(toolBar, BorderLayout.NORTH);
    }

    /**
     * 创建主面板
     */
    private void createMainPane() {
        this.leftPane = new LeftPane(ctx, this);
        this.rightPane = new RightPane(ctx, this);
        this.mainPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, this.leftPane, rightPane);
        this.mainPane.setDividerLocation(270);
        mainPane.setDividerSize(3);
        mainPane.setBorder(null); // 不设置该组件边框
        this.add(mainPane);
    }
}
