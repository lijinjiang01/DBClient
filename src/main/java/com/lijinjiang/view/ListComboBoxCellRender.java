package com.lijinjiang.view;

import com.lijinjiang.model.ViewObject;
import com.lijinjiang.model.tree.Database;
import com.lijinjiang.model.tree.ServerConnection;
import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.awt.*;

/**
 * @ClassName ListComboBoxCellRender
 * @Description 下拉选列表
 * @Author Li
 * @Date 2022/8/28 21:32
 * @ModifyDate 2022/8/28 21:32
 * @Version 1.0
 */
public class ListComboBoxCellRender extends DefaultListCellRenderer {
    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean cellHasFocus) {
        JLabel label = (JLabel) super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
        // 设置背景色
        Color focusGainedColor = new Color(205, 232, 255); // 聚焦时的背景色
        if (isSelected) {
            label.setBackground(focusGainedColor);
        }
        label.setOpaque(true);

        ViewObject object = (ViewObject) value;
        if (object == null) {
            return this;
        } else {
            label.setIcon(object.getIcon());
        }
        label.setPreferredSize(new Dimension(100, 40));
        return this;
    }
}
