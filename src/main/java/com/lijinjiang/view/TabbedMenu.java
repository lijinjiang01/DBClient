package com.lijinjiang.view;

import javax.swing.*;
import java.awt.event.MouseEvent;

/**
 * @ClassName TabbedMenu
 * @Description 右边面板选项卡的右键菜单
 * @Author Li
 * @Date 2022/8/22 23:41
 * @ModifyDate 2022/8/22 23:41
 * @Version 1.0
 */
public class TabbedMenu extends JPopupMenu {
    // 右边面板
    private RightPane rightPane;
    // 右键选中的选项卡索引
    private int selectIndex;
    // 选项卡数量
    private int tabCount;

    private JMenuItem closeTabItem = new JMenuItem("关闭", null);
    private JMenuItem closeOtherMenuItem = new JMenuItem("关闭其他", null);
    private JMenuItem closeAllMenuItem = new JMenuItem("全部关闭", null);
    private JMenuItem closeRightMenuItem = new JMenuItem("关闭右侧选项卡", null);


    public TabbedMenu(RightPane rightPane) {
        this.setFocusable(false);
        this.rightPane = rightPane;
        initTabbedMenuListeners(); // 添加右键菜单项监听
    }

    private void initTabbedMenuListeners() {
        // 关闭 操作
        this.closeTabItem.addActionListener(e -> closeTabAction());
        // 关闭其它 操作
        this.closeOtherMenuItem.addActionListener(e -> closeOtherTabAction());
        // 全部关闭 操作
        this.closeAllMenuItem.addActionListener(e -> closeAllTabAction());
        // 关闭右侧选项卡 操作
        this.closeRightMenuItem.addActionListener(e->closeRightTabAction());
    }

    // 关闭 操作
    private void closeTabAction() {
        rightPane.remove(selectIndex);
    }

    // 关闭其它 操作
    private void closeOtherTabAction() {
        if (selectIndex == 0) {
            for (int i = 1; i < tabCount; i++) {
                rightPane.removeTabAt(1);
            }
        } else if (selectIndex > 0) {
            for (int i = 1; i < selectIndex; i++) {
                rightPane.removeTabAt(1);
            }
            for (int i = selectIndex + 1; i < tabCount; i++) {
                rightPane.removeTabAt(2);
            }
        }
    }

    // 全部关闭 操作
    private void closeAllTabAction() {
        for (int i = 1; i < tabCount; i++) {
            rightPane.removeTabAt(1);
        }
    }

    // 关闭右侧选项卡 操作
    private void closeRightTabAction() {
        int index = selectIndex + 1;
        for (int i = index; i < tabCount; i++) {
            rightPane.removeTabAt(index);
        }
    }

    public void showTabbedMenu(MouseEvent e) {
        this.removeAll(); // 先移去全部组件
        createTabbedMenu();
        selectIndex = rightPane.indexAtLocation(e.getX(), e.getY()); // 获取鼠标右键的选项卡索引
        tabCount = rightPane.getTabCount();
        if (selectIndex == 0) {
            this.closeTabItem.setEnabled(false);
        } else if (selectIndex > 0) {
            this.closeTabItem.setEnabled(true);
        }
        // 如果只有一个选项卡，则不能全部关闭
        if (tabCount == 1) {
            this.closeOtherMenuItem.setEnabled(false);
            this.closeAllMenuItem.setEnabled(false);
            this.closeRightMenuItem.setEnabled(false);
        } else {
            this.closeOtherMenuItem.setEnabled(true);
            this.closeAllMenuItem.setEnabled(true);
            this.closeRightMenuItem.setEnabled(true);
        }
        this.show(e.getComponent(), e.getX(), e.getY()); // 右键菜单显示位置*/

    }

    // 创建右键菜单
    private void createTabbedMenu() {
        this.add(closeTabItem);
        this.add(new JPopupMenu.Separator());
        this.add(closeOtherMenuItem);
        this.add(closeAllMenuItem);
        this.add(new JPopupMenu.Separator());
        this.add(closeRightMenuItem);
    }
}
