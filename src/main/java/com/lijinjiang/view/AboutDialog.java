package com.lijinjiang.view;

import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.WindowEvent;
import java.awt.event.WindowFocusListener;

/**
 * @ClassName AboutDialog
 * @Description 关于页面
 * @Author Li
 * @Date 2022/8/15 13:00
 * @ModifyDate 2022/8/15 13:00
 * @Version 1.0
 */
public class AboutDialog extends JDialog {
    private MainFrame mainFrame; // 主界面对象

    public AboutDialog(MainFrame mainFrame) {
        this.mainFrame = mainFrame;
        this.setSize(new Dimension(600, 400)); // 设置大小
        this.setTitle("关于"); // 设置标题
        this.setUndecorated(true); // 禁用对话框的装饰

        JPanel contentPane = new JPanel();
        contentPane.setBackground(new Color(60, 60, 60));
        contentPane.setBorder(BorderFactory.createEtchedBorder());
        contentPane.setLayout(new GridBagLayout());

        JPanel panel = new JPanel();
        panel.setBackground(new Color(60, 60, 60));
        panel.setLayout(new GridLayout(2, 1, 0, 20));
        // 上半部分 LOGO + 名称
        JPanel topPanel = new JPanel();
        topPanel.setBackground(new Color(60, 60, 60));
        topPanel.setLayout(new FlowLayout());
        JLabel logoLabel = new JLabel(new ImageIcon(ImageUtil.LOGO_IMAGE.getScaledInstance(40, 40, Image.SCALE_SMOOTH)));
        JLabel nameLabel = new JLabel("DBClient");
        nameLabel.setFont(new Font("微软雅黑", 1, 48));
        nameLabel.setForeground(Color.WHITE);
        topPanel.add(logoLabel);
        topPanel.add(nameLabel);
        // 下半部分，作者
        JPanel bottomPanel = new JPanel();
        bottomPanel.setBackground(new Color(60, 60, 60));
        JLabel authorLabel = new JLabel("Author : lijinjiang01");
        authorLabel.setFont(new Font("微软雅黑", 1, 25));
        authorLabel.setForeground(Color.WHITE);
        bottomPanel.add(authorLabel);

        panel.add(topPanel);
        panel.add(bottomPanel);
        contentPane.add(panel);

        this.setContentPane(contentPane);
        this.setAlwaysOnTop(true); // 设置永远显示在最上面
        this.addWindowFocusListener(new WindowFocusListener() {
            @Override
            public void windowGainedFocus(WindowEvent e) {

            }

            @Override
            public void windowLostFocus(WindowEvent e) {
                // 当点击 JDialog 外的地方时，则关闭 JDialog 窗口
                AboutDialog.this.dispose();
            }
        });
        this.setResizable(false); // 不可变化大小
        this.setLocationRelativeTo(this.mainFrame); // 相对主界面居中
        this.setVisible(true); // 设置可见
    }
}
