package com.lijinjiang.view;

import com.lijinjiang.model.tree.Database;
import com.lijinjiang.model.tree.ServerConnection;
import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.util.Collections;
import java.util.List;

/**
 * @ClassName DBDialog
 * @Description 新建数据库界面
 * @Author Li
 * @Date 2022/8/23 23:18
 * @ModifyDate 2022/8/23 23:18
 * @Version 1.0
 */
public class DBDialog extends JDialog {
    // 服务器连接
    private ServerConnection sc;

    // 主界面
    private MainFrame mainFrame;

    // 标签及输入框
    private JLabel dbNameLabel = new JLabel("数据库名");
    private JTextField dbNameField = new JTextField();
    private JLabel charsetLabel = new JLabel("字 符 集");
    private JComboBox<String> charsetBox = new JComboBox<>();
    private JLabel collationLabel = new JLabel("排序规则");
    private JComboBox<String> collationBox = new JComboBox<>();
    // 操作按钮
    private JButton confirmBtn = new JButton("确定");
    private JButton cancelBtn = new JButton("取消");

    public DBDialog(ServerConnection sc, MainFrame frame) {
        this.sc = sc;
        this.mainFrame = frame;
        this.init();
        this.setLayout(null); // 布局格式为空布局
        this.setSize(new Dimension(400, 250));
        this.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE); // 设置默认关闭操作
        this.setLocationRelativeTo(mainFrame); // 设置相对传递的组件居中
        this.setIconImage(ImageUtil.DATABASE_IMAGE); // 设置logo
        this.setModal(true); // 设置为模态框
        this.setTitle("新建数据库");
        this.setVisible(true);
    }

    private void init() {
        // 把组件全部加到窗口上
        // 字符集下拉框初始化
        java.util.List<String> charsets = this.sc.getCharsets();
        Collections.sort(charsets); // 字符集列表排序
        for (String charset : charsets) {
            charsetBox.addItem(charset);
        }
        // 排序规则下拉框初始化
        Object selectedItem = charsetBox.getSelectedItem();
        java.util.List<String> collations = sc.getCollations(selectedItem.toString());
        // Collections.sort(collations); // 排序规则列表排序
        for (String collation : collations) {
            collationBox.addItem(collation);
        }
        // 字符集更改时，排序规则下拉动态变化
        charsetBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                collationBox.removeAllItems(); // 先移除所有的Item
                Object selectedItem = charsetBox.getSelectedItem();
                List<String> collations = sc.getCollations(selectedItem.toString());
                Collections.sort(collations); // 排序规则列表排序
                for (String collation : collations) {
                    collationBox.addItem(collation);
                }
            }
        });

        dbNameLabel.setBounds(30, 30, 62, 30);
        dbNameField.setBounds(100, 30, 250, 30);
        charsetLabel.setBounds(30, 70, 62, 30);
        charsetBox.setBounds(100, 70, 250, 30);
        collationLabel.setBounds(30, 110, 62, 30);
        collationBox.setBounds(100, 110, 250, 30);
        confirmBtn.setBounds(100, 160, 60, 30);
        cancelBtn.setBounds(240, 160, 60, 30);
        this.add(dbNameLabel);
        this.add(dbNameField);
        this.add(charsetLabel);
        this.add(charsetBox);
        this.add(collationLabel);
        this.add(collationBox);
        this.add(confirmBtn);
        this.add(cancelBtn);

        // 确定按钮
        confirmBtn.addActionListener(e -> addDatabaseAction());

        // 取消按钮：关闭该页面
        cancelBtn.addActionListener(e -> cancelAction());
    }

    // 新建数据库
    private void addDatabaseAction() {
        String dbName = dbNameField.getText();
        String charset = charsetBox.getSelectedItem().toString().trim();
        String collation = collationBox.getSelectedItem().toString().trim();
        if (dbName == null || dbName.isEmpty()) {
            JOptionPane.showMessageDialog(this, "请输入数据名称", "错误", JOptionPane.ERROR_MESSAGE);
            return;
        }
        Database database = new Database(dbName, charset, collation, this.sc);
        int index = this.sc.createDatabase(this, database);// 创建数据库后返回数据库的排序索引
        if (index > -1) {
            this.mainFrame.getLeftPane().addDatabaseNode(database, index);
        }
    }

    // 关闭页面
    private void cancelAction() {
        this.dispose();
    }
}
