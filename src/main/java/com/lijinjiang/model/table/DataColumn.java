package com.lijinjiang.model.table;

/**
 * @ClassName DataColumn
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:16
 * @ModifyDate 2022/8/15 16:16
 * @Version 1.0
 */
public class DataColumn {
    // 该列在Table中的索引
    private int index;
    // 该列的列名
    private String name;

    public DataColumn(int index, String name) {
        this.index = index;
        this.name = name;
    }

    public int getIndex() {
        return index;
    }

    public void setIndex(int index) {
        this.index = index;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return this.name;
    }
}
