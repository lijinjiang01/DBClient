package com.lijinjiang.model.table;

/**
 * @ClassName DataCell
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:16
 * @ModifyDate 2022/8/15 16:16
 * @Version 1.0
 */
public class DataCell {
    // 该单元格所在的行
    private int row;
    // 该单元格所在的列
    private DataColumn column;
    // 该单元格的值
    private String value;

    public DataCell(int row, DataColumn column, String value) {
        this.row = row;
        this.column = column;
        this.value = value;
    }

    public int getRow() {
        return row;
    }

    public void setRow(int row) {
        this.row = row;
    }

    public DataColumn getColumn() {
        return column;
    }

    public void setColumn(DataColumn column) {
        this.column = column;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}
