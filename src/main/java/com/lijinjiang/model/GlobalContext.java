package com.lijinjiang.model;

import com.lijinjiang.controller.PropertiesHandler;
import com.lijinjiang.controller.impl.PropertiesHandlerImpl;
import com.lijinjiang.model.tree.ServerConnection;
import com.lijinjiang.util.FileUtil;

import java.io.File;
import java.util.*;

/**
 * @ClassName GlobalContext
 * @Description 全局上下文
 * @Author Li
 * @Date 2022/8/15 12:41
 * @ModifyDate 2022/8/15 12:41
 * @Version 1.0
 */
public class GlobalContext {

    // 属性文件处理接口
    private PropertiesHandler propertiesHandler = new PropertiesHandlerImpl();

    public PropertiesHandler getPropertiesHandler() {
        return this.propertiesHandler;
    }

    // 存放所有服务器连接的集合
    private Map<String, ServerConnection> scMap = new HashMap<>();

    // 存放按序存储的名字集合
    public  List<String> getSortNameList(){
        Set<String> keys = scMap.keySet();
        List<String> list = new ArrayList<>();
        for (String key : keys) {
            list.add(key);
        }
        Collections.sort(list, new Comparator<String>() {
            @Override
            public int compare(String o1, String o2) {
                String s1 = o1.toLowerCase();
                String s2 = o2.toLowerCase();

                byte[] b1 = s1.getBytes();
                byte[] b2 = s2.getBytes();
                int len = b1.length < b2.length ? b1.length : b2.length;
                for (int i = 0; i < len; i++) {
                    if (b1[i] != b2[i]) {
                        return b1[i] - b2[i];
                    }
                }
                return b1.length - b2.length;
            }
        });
        return list;
    }

    // 根据连接名获取一个 ServerConnection
    public ServerConnection getSC(String name) {
        return this.scMap.get(name);
    }

    // 添加一个连接到 Map 集合
    public void addSC(ServerConnection sc) {
        this.scMap.put(sc.getName(), sc);
    }

    // 获取所有的 ServerConnection
    public Map<String, ServerConnection> getAllSC() {
        return this.scMap;
    }

    // 从 Map 中删除一个连接
    public void removeSC(ServerConnection sc) {
        // 删除该服务器连接的配置文件
        File profile = new File(FileUtil.SC_PATH + sc.getName() + ".properties");
        profile.delete();
        // 从集合移除
        this.scMap.remove(sc.getName());
    }
}
