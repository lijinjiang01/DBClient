package com.lijinjiang.model;

import com.lijinjiang.exception.QueryException;

import java.sql.ResultSet;

/**
 * @ClassName QueryObject
 * @Description 查询数据接口
 * @Author Li
 * @Date 2022/8/21 23:18
 * @ModifyDate 2022/8/21 23:18
 * @Version 1.0
 */
public interface QueryObject {

    /**
     * 获取查询数据
     */
    ResultSet getData() throws QueryException;
}
