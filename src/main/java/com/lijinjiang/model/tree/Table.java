package com.lijinjiang.model.tree;

import com.lijinjiang.exception.QueryException;
import com.lijinjiang.model.QueryData;
import com.lijinjiang.model.ViewObject;
import com.lijinjiang.model.table.DataCell;
import com.lijinjiang.model.table.DataColumn;
import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Table
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:15
 * @ModifyDate 2022/8/15 16:15
 * @Version 1.0
 */
public class Table implements ViewObject {

    // 表单名称
    private String tableName;

    // 所属的数据库节点
    private Database database;

    // 当前的ResultSet对象
    private ResultSet rs;


    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    // 构造方法
    public Table(Database database, String tableName) {
        this.database = database;
        this.tableName = tableName;
    }

    @Override
    public Icon getIcon() {
        return ImageUtil.TABLE_ICON;
    }

    public String toString() {
        return this.tableName;
    }

}
