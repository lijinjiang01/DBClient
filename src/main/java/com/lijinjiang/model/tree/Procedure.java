package com.lijinjiang.model.tree;

import com.lijinjiang.model.ViewObject;
import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.sql.Connection;

/**
 * @ClassName Procedure
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:14
 * @ModifyDate 2022/8/15 16:14
 * @Version 1.0
 */
public class Procedure implements ViewObject {
    private String procedureName;

    public String getProcedureName() {
        return procedureName;
    }

    public void setProcedureName(String procedureName) {
        this.procedureName = procedureName;
    }

    private Database database;

    public Procedure(Database database, String procedureName) {
        this.database = database;
        this.procedureName = procedureName;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    @Override
    public Icon getIcon() {
        return ImageUtil.PROCEDURE_ICON;
    }

    public String toString() {
        return this.procedureName;
    }

}
