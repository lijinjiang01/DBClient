package com.lijinjiang.model.tree;

import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.sql.Connection;

/**
 * @ClassName ViewGroup
 * @Description 数据库结构的视图分类
 * @Author Li
 * @Date 2022/8/19 22:38
 * @ModifyDate 2022/8/19 22:38
 * @Version 1.0
 */
public class ViewGroup extends ConnectionNode{
    // 所属的数据库节点
    private Database database;

    // 数据库所属的服务器连接
    private ServerConnection sc;

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    public ServerConnection getSc() {
        return sc;
    }

    public void setSc(ServerConnection sc) {
        this.sc = sc;
    }

    public ViewGroup(Database database) {
        this.database = database;
        this.sc = database.getSc();
    }

    @Override
    public Connection connect() {
        // 如果已连接，则返回
        if (super.connection != null) return super.connection;
        // 创建数据库连接
        try {
            super.connection = this.sc.createConnection(database.getDbName());
        } catch (Exception e) {
            throw new RuntimeException("创建数据库连接异常！");
        }
        return super.connection;
    }

    @Override
    public Icon getIcon() {
        return ImageUtil.VIEW_ICON;
    }

    public String toString() {
        return "视图";
    }
}
