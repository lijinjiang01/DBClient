package com.lijinjiang.model.tree;

import com.lijinjiang.model.ViewObject;
import com.lijinjiang.util.ImageUtil;

import javax.swing.*;
import java.sql.Connection;

/**
 * @ClassName View
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:15
 * @ModifyDate 2022/8/15 16:15
 * @Version 1.0
 */
public class View implements ViewObject {
    private String viewName; // 视图名称

    public String getViewName() {
        return viewName;
    }

    public void setViewName(String viewName) {
        this.viewName = viewName;
    }

    private Database database;

    public View(Database database, String viewName) {
        this.database = database;
        this.viewName = viewName;
    }

    public Database getDatabase() {
        return database;
    }

    public void setDatabase(Database database) {
        this.database = database;
    }

    @Override
    public Icon getIcon() {
        return ImageUtil.VIEW_ICON;
    }

    public String toString() {
        return this.viewName;
    }

}
