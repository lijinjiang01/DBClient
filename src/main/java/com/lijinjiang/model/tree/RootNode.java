package com.lijinjiang.model.tree;

import com.lijinjiang.model.ViewObject;

import javax.swing.*;

/**
 * @ClassName RootNode
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 14:08
 * @ModifyDate 2022/8/15 14:08
 * @Version 1.0
 */
public class RootNode implements ViewObject {
    @Override
    public Icon getIcon() {
        return null;
    }
}
