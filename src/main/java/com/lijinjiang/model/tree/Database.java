package com.lijinjiang.model.tree;

import com.lijinjiang.util.ImageUtil;
import com.lijinjiang.util.MySQLUtil;

import javax.swing.*;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 * @ClassName Database
 * @Description TODO
 * @Author Li
 * @Date 2022/8/15 16:14
 * @ModifyDate 2022/8/15 16:14
 * @Version 1.0
 */
public class Database extends ConnectionNode {

    // 数据库名字
    private String dbName;

    // 字符集
    private String charset;

    // 排序规则
    private String collation;

    // 数据库所属的服务器连接
    private ServerConnection sc;

    // Statement接口
    private Statement stmt;

    public String getDbName() {
        return dbName;
    }

    public void setDbName(String dbName) {
        this.dbName = dbName;
    }

    public String getCharset() {
        return charset;
    }

    public void setCharset(String charset) {
        this.charset = charset;
    }

    public String getCollation() {
        return collation;
    }

    public void setCollation(String collation) {
        this.collation = collation;
    }

    public ServerConnection getSc() {
        return sc;
    }

    public void setSc(ServerConnection sc) {
        this.sc = sc;
    }

    public Database(String dbName, ServerConnection SConnection) {
        this.dbName = dbName;
        this.sc = SConnection;
    }

    public Database(String dbName, String charset, String collation, ServerConnection SConnection) {
        this.dbName = dbName;
        this.charset = charset;
        this.collation = collation;
        this.sc = SConnection;
    }

    @Override
    public Connection connect() {
        // 如果已连接，则返回
        if (super.connection != null) return super.connection;
        // 创建数据库连接
        try {
            super.connection = this.sc.createConnection(dbName);
        } catch (Exception e) {
            throw new RuntimeException("创建数据库连接异常！");
        }
        return super.connection;
    }

    // 获取Statement接口对象
    public Statement getStatement() throws SQLException {
        if (this.stmt == null) {
            this.stmt = super.connection.createStatement();
        }
        return this.stmt;
    }

    // 获取该数据库下的所有的表单
    public List<Table> getTables() {
        List<Table> list = new ArrayList<>();
        // 去系统表information_schema查询
        String sql = "SELECT TABLE_NAME FROM information_schema.TABLES sc WHERE (sc.TABLE_TYPE='" + MySQLUtil.TABLE_TYPE
                + "' OR sc.TABLE_TYPE='" + MySQLUtil.SYSTEM_VIEW_TYPE + "') AND sc.TABLE_SCHEMA='" + this.dbName
                + "' ORDER BY TABLE_NAME";
        try {
            // 获得本连接下面的所有数据库
            Statement stmt = getStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String tableName = rs.getString("TABLE_NAME");
                Table table = new Table(this, tableName);
                list.add(table);
            }
            rs.close();
            return list;
        } catch (Exception e) {
            return list;
        }
    }

    // 获取该数据库下的所有的视图
    public List<View> getViews() {
        List<View> list = new ArrayList<>();
        // 到 information_schema 数据库中的 VIEWS 表查询
        String sql = "SELECT * FROM information_schema.VIEWS sc WHERE sc.TABLE_SCHEMA='" + this.dbName
                + "' ORDER BY TABLE_NAME";
        try {
            // 获得本连接下面的所有数据库
            Statement stmt = getStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String viewName = rs.getString("TABLE_NAME");
                View view = new View(this, viewName);
                list.add(view);
            }
            rs.close();
            return list;
        } catch (Exception e) {
            return list;
        }
    }

    // 获取所有的存储过程
    public List<Procedure> getProcedures() {
        List<Procedure> list = new ArrayList<>();
        // 去系统表information_schema查询
        String sql = "SELECT * FROM information_schema.Routines WHERE ROUTINE_SCHEMA='" + dbName
                + "' AND ROUTINE_TYPE='" + MySQLUtil.PROCEDURE_TYPE + "' ORDER BY ROUTINE_NAME";
        try {
            // 获得一个连接下面的所有数据库
            Statement stmt = getStatement();
            ResultSet rs = stmt.executeQuery(sql);
            while (rs.next()) {
                String procedureName = rs.getString("ROUTINE_NAME");
                Procedure procedure = new Procedure(this, procedureName);
                list.add(procedure);
            }
            rs.close();
            return list;
        } catch (Exception e) {
            return list;
        }
    }

    // 自定义图标
    @Override
    public Icon getIcon() {
        if (this.connection == null) return ImageUtil.DATABASE_CLOSE_ICON;
        return ImageUtil.DATABASE_OPEN_ICON;
    }

    // 重写toString方法，树节点直接显示数据库名称
    public String toString() {
        return this.dbName;
    }
}
