package com.lijinjiang.model.tree;

import com.lijinjiang.model.ViewObject;

import java.sql.Connection;

/**
 * @ClassName ConnectionNode
 * @Description 需要进行连接的节点的父类， Database 与 ServerConnection
 * @Author Li
 * @Date 2022/8/15 10:53
 * @ModifyDate 2022/8/15 10:53
 * @Version 1.0
 */
public abstract class ConnectionNode implements ViewObject {
    protected Connection connection; // 连接

    public abstract Connection connect();

    public Connection getConnection() {
        return this.connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
