package com.lijinjiang.model;

import javax.swing.*;

/**
 * @ClassName ViewObject
 * @Description 外观显示的接口，让树节点接口和JList数据的接口继承
 * @Author Li
 * @Date 2022/8/15 10:52
 * @ModifyDate 2022/8/15 10:52
 * @Version 1.0
 */
public interface ViewObject {

    /**
     * 返回显示的图片
     */
    Icon getIcon();
}
