package com.lijinjiang.controller;

import com.lijinjiang.model.tree.ServerConnection;

import java.util.List;

/**
 * @ClassName PropertiesHandler
 * @Description properties 文件处理接口
 * @Author Li
 * @Date 2022/8/15 14:13
 * @ModifyDate 2022/8/15 14:13
 * @Version 1.0
 */
public interface PropertiesHandler {
    /**
     * 将一个ServerConnection对象保存到系统的properties文件中
     */
    void saveSC(ServerConnection sc);

    /**
     * 从本地的 profile 目录读取全部的配置文件，并封装成 ServerConnection 并返回
     */
    List<ServerConnection> getAllSC();
}
