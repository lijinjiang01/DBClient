package com.lijinjiang;

import org.junit.Test;

import javax.swing.*;

/**
 * @ClassName MainTest
 * @Description TODO
 * @Author Li
 * @Date 2022/8/24 14:48
 * @ModifyDate 2022/8/24 14:48
 * @Version 1.0
 */
public class MainTest {
    @Test
    public void showLookAndFeel() {
        UIManager.LookAndFeelInfo[] lookAndFeels = UIManager.getInstalledLookAndFeels();
        for (UIManager.LookAndFeelInfo lookAndFeel : lookAndFeels) {
            System.out.println(lookAndFeel.getName());
            System.out.println(lookAndFeel.getClassName());
        }
    }
}
